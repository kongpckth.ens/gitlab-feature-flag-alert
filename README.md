# Gitlab::FeatureFlagAlert

This tool looks for outdated feature flags in the `gitlab-org/gitlab` project and creates an issue for each GitLab group with outdated feature flags.

A feature flag is outdated when it is a `development` feature flag that has had `dafault_enabled: true` for at least 2 milestones.

Issues are then created for each GitLab group (team) that lists all their outdated feature flags.
The issues are assigned to each group's Engineering Manager.

The issues do not include:

- Feature flags that currently have `default_enabled: false` (even if 100% enabled on production).
- Feature flags created in the last 2 milestones.
- Feature flags of types other than `development`. For example, `ops` feature flags are not included.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'gitlab-feature_flag_alert'
```

And then execute:

```
bundle install
```

Or install it yourself as:

```
gem install gitlab-feature_flag_alert
```

## Usage

Set ENV variables:

- (required) `ENV['GITLAB_TOKEN']`: Your access token for access to the API.
- (optional) `ENV['CREATE_ISSUE']`: `true`to create 1 issue per group.
- (optional) `ENV['GITLAB_TRIAGE_PROJECT_ID']`: Created the issues in this project.

To run the tool on the CLI:

```
feature-flag-alert
```

To run the tool as a Gem:

```ruby
Gitlab::FeatureFlagAlert::Monitor.new.run
```

## Contributing

Bug reports and merge requests are welcome on GitLab at https://gitlab.com/fabiopitino/gitlab-feature-flag-alert

The tool was originally implemented by [@fabiopitino](https://gitlab.com/fabiopitino) and is now
maintained by the [Engineering Productivity team](https://about.gitlab.com/handbook/engineering/quality/engineering-productivity-team/).

