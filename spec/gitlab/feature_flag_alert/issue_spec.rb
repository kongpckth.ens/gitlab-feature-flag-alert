# frozen_string_literal: true

require "spec_helper"

RSpec.describe Gitlab::FeatureFlagAlert::Issue do
  let(:group) { "group::code review" }
  let(:flags) do
    {
      needs_action: [
        Gitlab::FeatureFlagAlert::FeatureFlag.new({
          "name" => "flag_one",
          "introduced_by_url" => "http://merge_request.com",
          "rollout_issue_url" => "http://issue.com",
          "milestone" => "11.7",
          "type" => "development",
          "group" => "group::code review",
          "default_enabled" => true
        })
      ],
      overdue: [
        Gitlab::FeatureFlagAlert::FeatureFlag.new({
          "name" => "flag_two",
          "introduced_by_url" => nil,
          "rollout_issue_url" => nil,
          "milestone" => "12.0",
          "type" => "development",
          "group" => "group::code review",
          "default_enabled" => false
        }),
        Gitlab::FeatureFlagAlert::FeatureFlag.new({
          "name" => "flag_three",
          "introduced_by_url" => nil,
          "rollout_issue_url" => "http://nope.com",
          "milestone" => "12.0",
          "type" => "development",
          "group" => "group::code review",
          "default_enabled" => true
        })
      ]
    }
  end

  subject { Gitlab::FeatureFlagAlert::Issue.new(group, flags, nil) }

  describe "#prepared_flags" do
    it "creates checkboxes for each flag" do
      expectation = ["- [ ] `flag_two` - %12.0 *Missing rollout issue", "- [ ] `flag_three` - %12.0 http://nope.com"]

      expect(subject.prepared_flags(flags[:overdue])).to eq(expectation)
    end
  end

  describe "#issue_body" do
    it "returns a bunch of text" do
      expectation = <<~STRING
        This is a group level feature flag report containing feature flags that should be evaluated or need action.

        Feature flag trends can be found in the [Sisense dashboard.](https://app.periscopedata.com/app/gitlab/792066/Engineering-::-Feature-Flags)

        # Feature flags needing action

        These flags have been enabled in the codebase for 6 or more releases.

        ----

        - [ ] `flag_one` - %11.7 http://issue.com

        ----

        Please take action on these feature flags by performing one of the following options:

        1. Enable the feature flag by default and remove it.
        1. Convert it to an instance, group, or project setting.
        1. Revert the changes if it's still disabled and not needed anymore.

        # Feature flags overdue

        These flags have been enabled in the codebase for 2 or more releases.

        ----

        - [ ] `flag_two` - %12.0 *Missing rollout issue
        - [ ] `flag_three` - %12.0 http://nope.com

        ----

        Please review these feature flags to determine if they are able to be removed entirely.

        This report is generated from [feature-flag-alert project](https://gitlab.com/gitlab-org/gitlab-feature-flag-alert/)

        /label ~"group::code review"
        /assign @gl_quality/eng-prod
      STRING

      expect(subject.body).to eq(expectation)
    end
  end

  describe "#title" do
    it "includes group and date" do
      expect(subject.title).to include(Date.today.iso8601)
      expect(subject.title).to include("group::code review")
    end
  end
end
