# frozen_string_literal: true

require "spec_helper"

RSpec.describe Gitlab::FeatureFlagAlert::Milestone do
  describe "#ago" do
    it "returns the milestone title from N milestones ago" do
      milestone = described_class.new("13.0")

      old_milestone = milestone.ago(4)

      expect(old_milestone).to eq("12.9")
    end
  end

  describe "#<=>" do
    it "compares milestones as gitlab versions" do
      milestone = described_class.new("13.11")

      expect(milestone).to be < "14.0"
      expect(milestone).to be > "13.10"
      expect(milestone).to eq "13.11"
    end
  end
end
