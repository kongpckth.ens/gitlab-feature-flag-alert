# frozen_string_literal: true

require "spec_helper"

RSpec.describe Gitlab::FeatureFlagAlert::Monitor do
  let(:monitor) { described_class.new }
  let(:gitlab_service) { double(:gitlab_service) }

  before do
    allow(monitor).to receive(:gitlab).and_return(gitlab_service)
  end

  describe "#run" do
    subject { monitor.run }

    let(:feature_flags) do
      Gitlab::FeatureFlagAlert::FeatureFlagCollection.new(
        [
          Gitlab::FeatureFlagAlert::FeatureFlag.new(
            "name" => "needs_action_feature_flag",
            "introduced_by_url" => "",
            "rollout_issue_url" => "",
            "milestone" => "13.1",
            "type" => "development",
            "group" => "group::code review",
            "default_enabled" => true
          ),
          Gitlab::FeatureFlagAlert::FeatureFlag.new(
            "name" => "overdue_feature_flag",
            "introduced_by_url" => "",
            "rollout_issue_url" => "",
            "milestone" => "13.10",
            "type" => "development",
            "group" => "group::code review",
            "default_enabled" => true
          ),
          Gitlab::FeatureFlagAlert::FeatureFlag.new(
            "name" => "old_disabled_feature_flag",
            "introduced_by_url" => "",
            "rollout_issue_url" => "",
            "milestone" => "13.10",
            "type" => "development",
            "group" => "group::code review",
            "default_enabled" => false
          ),
          Gitlab::FeatureFlagAlert::FeatureFlag.new(
            "name" => "new_feature_flag",
            "introduced_by_url" => "",
            "rollout_issue_url" => "",
            "milestone" => "14.0",
            "type" => "development",
            "group" => "group::pipeline execution",
            "default_enabled" => true
          ),
          Gitlab::FeatureFlagAlert::FeatureFlag.new(
            "name" => "new_feature_flag",
            "introduced_by_url" => "",
            "rollout_issue_url" => "",
            "milestone" => "13.9",
            "type" => "development",
            "group" => "group::pipeline authoring",
            "default_enabled" => true
          )
        ]
      )
    end

    before do
      allow(gitlab_service).to receive(:current_milestone).and_return({ "title" => "14.0" })
      allow(monitor).to receive(:feature_flags).and_return(feature_flags)
      allow(gitlab_service).to receive(:retrieve_assignees).and_return(assignees_by_group)
    end

    shared_examples "creates one issue for group with overdue flag" do |assignees|
      it "creates issue with correct assignees" do
        issues = subject

        expect(issues.count).to eq(2)
        issue = issues.first

        expect(issue.group).to eq("group::code review")
        expect(issue.flags[:overdue].map(&:name)).to eq(%w[overdue_feature_flag old_disabled_feature_flag])
        expect(issue.flags[:needs_action].map(&:name)).to eq(%w[needs_action_feature_flag])
        expect(issue.assignees).to eq(assignees)
      end
    end

    context "when group has backend_engineering_manager" do
      let(:assignees_by_group) do
        {
          "code_review" => {
            "backend_engineering_manager" => ["@alice", "@bob"]
          }
        }
      end

      it_behaves_like "creates one issue for group with overdue flag", "@alice @bob"
    end
  end
end
