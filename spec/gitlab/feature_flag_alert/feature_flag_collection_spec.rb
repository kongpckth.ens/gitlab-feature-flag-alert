# frozen_string_literal: true

require "spec_helper"

RSpec.describe Gitlab::FeatureFlagAlert::FeatureFlagCollection do
  it "is enumerates its flags" do
    flag_one = double(:flag_one, test: nil)
    flag_two = double(:flag_two, test: nil)
    collection = described_class.new([flag_one, flag_two])

    collection.each(&:test)

    expect(flag_one).to have_received(:test).once
    expect(flag_two).to have_received(:test).once
  end

  describe "#created_between" do
    it "returns flags created between the given milestones" do
      older_flag = Gitlab::FeatureFlagAlert::FeatureFlag.new(
        "name" => "needs_action_feature_flag",
        "introduced_by_url" => "",
        "rollout_issue_url" => "",
        "milestone" => "13.5",
        "type" => "development",
        "group" => "group::code review",
        "default_enabled" => true
      )
      thirteen_six_flag = Gitlab::FeatureFlagAlert::FeatureFlag.new(
        "name" => "needs_action_feature_flag",
        "introduced_by_url" => "",
        "rollout_issue_url" => "",
        "milestone" => "13.6",
        "type" => "development",
        "group" => "group::code review",
        "default_enabled" => true
      )
      thirteen_seven_flag = Gitlab::FeatureFlagAlert::FeatureFlag.new(
        "name" => "needs_action_feature_flag",
        "introduced_by_url" => "",
        "rollout_issue_url" => "",
        "milestone" => "13.7",
        "type" => "development",
        "group" => "group::code review",
        "default_enabled" => true
      )
      new_flag = Gitlab::FeatureFlagAlert::FeatureFlag.new(
        "name" => "old_disabled_feature_flag",
        "introduced_by_url" => "",
        "rollout_issue_url" => "",
        "milestone" => "13.10",
        "type" => "development",
        "group" => "group::code review",
        "default_enabled" => false
      )
      flags = [thirteen_seven_flag, thirteen_six_flag, older_flag, new_flag]
      collection = described_class.new(flags)

      between_flags = collection.created_between("13.6", "13.9")

      expect(between_flags).to contain_exactly(thirteen_six_flag, thirteen_seven_flag)
    end
  end

  describe "#older_than" do
    it "returns flags created before the given milestone" do
      older_flag = Gitlab::FeatureFlagAlert::FeatureFlag.new(
        "name" => "needs_action_feature_flag",
        "introduced_by_url" => "",
        "rollout_issue_url" => "",
        "milestone" => "13.5",
        "type" => "development",
        "group" => "group::code review",
        "default_enabled" => true
      )
      new_flag = Gitlab::FeatureFlagAlert::FeatureFlag.new(
        "name" => "old_disabled_feature_flag",
        "introduced_by_url" => "",
        "rollout_issue_url" => "",
        "milestone" => "13.10",
        "type" => "development",
        "group" => "group::code review",
        "default_enabled" => false
      )
      flags = [older_flag, new_flag]
      collection = described_class.new(flags)

      older_flags = collection.older_than("13.8")

      expect(older_flags).to contain_exactly(older_flag)
    end
  end

  describe "#development" do
    it "returns development flags" do
      development_flag = Gitlab::FeatureFlagAlert::FeatureFlag.new(
        "name" => "needs_action_feature_flag",
        "introduced_by_url" => "",
        "rollout_issue_url" => "",
        "milestone" => "13.5",
        "type" => "development",
        "group" => "group::code review",
        "default_enabled" => true
      )
      ops_flag =  Gitlab::FeatureFlagAlert::FeatureFlag.new(
        "name" => "overdue_feature_flag",
        "introduced_by_url" => "",
        "rollout_issue_url" => "",
        "milestone" => "13.7",
        "type" => "ops",
        "group" => "group::code review",
        "default_enabled" => true
      )
      flags = [development_flag, ops_flag]
      collection = described_class.new(flags)

      development_flags = collection.development

      expect(development_flags).to contain_exactly(development_flag)
    end
  end
end
