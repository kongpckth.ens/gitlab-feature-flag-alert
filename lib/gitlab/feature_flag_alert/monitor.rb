# frozen_string_literal: true

require "gitlab/feature_flag_alert/feature_flag"
require "gitlab/feature_flag_alert/gitlab_service"
require "gitlab/feature_flag_alert/issue"
require "gitlab/feature_flag_alert/milestone"

module Gitlab
  module FeatureFlagAlert
    class Monitor
      GITLAB_GIT_REPO = "https://gitlab.com/gitlab-org/gitlab.git"
      NUM_MILESTONES_FLAG_NEEDS_ACTION = 6
      NUM_MILESTONES_FLAG_OVERDUE = 2
      WORK_DIR = "gitlab-repo"

      def run
        update_repository

        current_milestone = Milestone.new(gitlab.current_milestone["title"])
        puts "Current milestone: #{current_milestone}"

        milestone_needing_action = current_milestone.ago(NUM_MILESTONES_FLAG_NEEDS_ACTION)
        puts "Needs action milestone: #{milestone_needing_action}"

        milestone_overdue = current_milestone.ago(NUM_MILESTONES_FLAG_OVERDUE)
        puts "Overdue milestone: #{milestone_overdue}"

        flags_needing_action_by_group = feature_flags
                                        .development
                                        .older_than(milestone_needing_action)
                                        .group_by(&:group)
        flags_overdue_by_group = feature_flags
                                 .development
                                 .created_between(milestone_needing_action, milestone_overdue)
                                 .group_by(&:group)
        groups = (flags_needing_action_by_group.keys + flags_overdue_by_group.keys).uniq

        flags_by_group = groups.reduce({}) do |flags, group|
          flags.update(
            group => {
              needs_action: flags_needing_action_by_group.fetch(group, []),
              overdue: flags_overdue_by_group.fetch(group, [])
            }
          )
        end

        flags_by_group.map do |group, flags|
          flags_needing_action = flags[:needs_action]
          flags_overdue = flags[:overdue]

          puts "Feature flags needing action for #{group}:"
          flags_needing_action.each do |flag|
            puts " * #{flag.name} - #{flag.milestone}"
          end

          puts ""

          puts "Feature flags overdue for #{group}:"
          flags_overdue.each do |flag|
            puts " * #{flag.name} - #{flag.milestone}"
          end

          puts ""

          create_issue(group, flags)
        end
      end

      private

      def feature_flags
        @feature_flags ||= FeatureFlag.load_all(File.join(WORK_DIR, "config/feature_flags/**/*.yml"))
      end

      def gitlab
        @gitlab ||= GitlabService.new
      end

      def assignees
        @assignees ||= gitlab.retrieve_assignees
      end

      def update_repository
        `git clone --depth=1 #{GITLAB_GIT_REPO} #{WORK_DIR} || (cd #{WORK_DIR} && git pull origin master)`
      end

      def prepared_assignees(group)
        # Unknown group
        return if group.nil?

        group = group.dup
        group.slice!("group::")
        group = group.gsub(" ", "_")

        prepared = [
          assignees.dig(group, "backend_engineering_manager"),
          assignees.dig(group, "frontend_engineering_manager"),
          assignees.dig(group, "engineering_manager")
        ].compact

        # No DRI
        return if prepared.empty?

        prepared.join(" ")
      end

      def create_issue(group, flags)
        Issue.new(group, flags, prepared_assignees(group)).tap do |issue|
          gitlab.create_issue(issue) if ENV["CREATE_ISSUE"] == "true"
        end
      end
    end
  end
end
