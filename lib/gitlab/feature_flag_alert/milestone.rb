# frozen_string_literal: true

module Gitlab
  module FeatureFlagAlert
    class Milestone
      include Comparable

      def initialize(value)
        @value = value
      end

      def ago(num_milestones)
        major, minor = value.split(".").map(&:to_i)

        older_major = minor > 1 ? major : major - 1
        older_minor = (0..12).to_a[minor - num_milestones]

        [older_major, older_minor].join(".")
      end

      def <=>(other)
        Gem::Version.new(@value) <=> Gem::Version.new(other.to_s)
      end

      def inspect
        @value
      end
      alias to_s inspect

      private

      attr_reader :value
    end
  end
end
