# frozen_string_literal: true

require "yaml"
require "gitlab/feature_flag_alert/feature_flag_collection"
require "gitlab/feature_flag_alert/milestone"

module Gitlab
  module FeatureFlagAlert
    class FeatureFlag
      ATTRIBUTES = %i[name introduced_by_url rollout_issue_url type group default_enabled].freeze

      attr_accessor(*ATTRIBUTES)

      def self.load_all(dir_path)
        flags = Dir.glob(dir_path).map do |path|
          params = YAML.load_file(path)

          FeatureFlag.new(params)
        end

        FeatureFlagCollection.new(flags)
      end

      def initialize(params)
        ATTRIBUTES.each do |attribute|
          public_send("#{attribute}=", params[attribute.to_s])
        end

        @params = params
      end

      def milestone
        Milestone.new(params["milestone"])
      end

      private

      attr_reader :params
    end
  end
end
