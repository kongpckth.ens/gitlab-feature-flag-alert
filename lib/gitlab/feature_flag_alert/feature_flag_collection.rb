# frozen_string_literal: true

module Gitlab
  module FeatureFlagAlert
    class FeatureFlagCollection
      include Enumerable

      def initialize(flags)
        @flags = flags
      end

      def created_between(older_milestone, newer_milestone)
        self.class.new(flags_created_between(older_milestone, newer_milestone))
      end

      def older_than(milestone)
        self.class.new(flags_older_than(milestone))
      end

      def development
        self.class.new(development_flags)
      end

      def each(&block)
        flags.each do |flag|
          block.call(flag)
        end
      end

      private

      attr_reader :flags

      def development_flags
        flags.select do |flag|
          flag.type == "development"
        end
      end

      def flags_older_than(milestone)
        flags.select do |flag|
          flag.milestone && flag.milestone < milestone
        end
      end

      def flags_created_between(older_milestone, newer_milestone)
        flags.select do |flag|
          flag.milestone &&
            older_milestone <= flag.milestone &&
            flag.milestone < newer_milestone
        end
      end
    end
  end
end
