# frozen_string_literal: true

require "date"

module Gitlab
  module FeatureFlagAlert
    class Issue
      DASHBOARD_URL = "https://app.periscopedata.com/app/gitlab/792066/Engineering-::-Feature-Flags"
      DEFAULT_ASSIGNEE = "@gl_quality/eng-prod"

      attr_reader :group, :flags, :assignees

      def initialize(group, flags, assignees)
        @group = group
        @flags = flags
        @assignees = assignees || DEFAULT_ASSIGNEE
      end

      def title
        "Feature flags requiring attention for #{group} - #{Date.today.iso8601}"
      end

      def prepared_flags(flags_to_prepare)
        content = []
        flags_to_prepare.each do |flag|
          content << "- [ ] `#{flag.name}` - %#{flag.milestone} #{flag.rollout_issue_url || '*Missing rollout issue'}"
        end
        content
      end

      def body
        <<~SUMMARY
          This is a group level feature flag report containing feature flags that should be evaluated or need action.

          Feature flag trends can be found in the [Sisense dashboard.](https://app.periscopedata.com/app/gitlab/792066/Engineering-::-Feature-Flags)

          # Feature flags needing action

          These flags have been enabled in the codebase for #{Monitor::NUM_MILESTONES_FLAG_NEEDS_ACTION} or more releases.

          ----

          #{prepared_flags(flags[:needs_action]).join("\n")}

          ----

          Please take action on these feature flags by performing one of the following options:

          1. Enable the feature flag by default and remove it.
          1. Convert it to an instance, group, or project setting.
          1. Revert the changes if it's still disabled and not needed anymore.

          # Feature flags overdue

          These flags have been enabled in the codebase for #{Monitor::NUM_MILESTONES_FLAG_OVERDUE} or more releases.

          ----

          #{prepared_flags(flags[:overdue]).join("\n")}

          ----

          Please review these feature flags to determine if they are able to be removed entirely.

          This report is generated from [feature-flag-alert project](https://gitlab.com/gitlab-org/gitlab-feature-flag-alert/)

          /label ~"#{group}"
          /assign #{assignees}
        SUMMARY
      end
    end
  end
end
