# frozen_string_literal: true

require 'faraday'
require 'json'
require 'yaml'
require 'date'

module Gitlab
  module FeatureFlagAlert
    class GitlabService
      BASE_URL = 'https://gitlab.com/api/v4'
      GITLAB_ORG_GROUP_ID = 9970
      GITLAB_GROUP_DEFINITIONS = 'https://gitlab.com/gitlab-org/quality/triage-ops/-/raw/master/group-definition.yml'
      GITLAB_TRIAGE_PROJECT_ID = ENV['TRIAGE_PROJECT_ID']

      def current_milestone
        response = Faraday.get(BASE_URL + "/groups/#{GITLAB_ORG_GROUP_ID}/milestones", { state: :active }, http_headers)

        raise Error, response.body unless response.success?

        milestones = JSON.parse(response.body)

        milestones.find do |milestone|
          !milestone['expired'] &&
          milestone['start_date'] && milestone['due_date'] &&
          Date.parse(milestone['start_date']) < Date.today &&
          Date.parse(milestone['due_date']) > Date.today
        end
      end

      def retrieve_assignees
        response = Faraday.get(GITLAB_GROUP_DEFINITIONS, http_headers)

        raise Error, response.body unless response.success?

        YAML.load(response.body)
      end

      def create_issue(issue)
        response = Faraday.post(BASE_URL + "/projects/#{GITLAB_TRIAGE_PROJECT_ID}/issues", 
          { 
            description: issue.body,
            title: issue.title
          }, http_headers)

        raise Error, response.body unless response.success?

        JSON.parse(response.body)
      end

      private

      def http_headers
        { 'PRIVATE-TOKEN' => ENV['GITLAB_TOKEN'] }
      end
    end
  end
end
